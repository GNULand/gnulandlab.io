# نسخه‌ی ۲.۳ تراکتور

نگارش ۲.۳ تراکتور رو دیروز منتشر کردیم.  
در این نگارش تغییرات خوبی رو با کمک یکی از دوستان خوبمون داشتیم.

پنل تراکتور\(همون indicator\) رو دوست خوبمون [احسان](http://ehsaan.me) لطف کرد و از اول بازنویسی کرد و ما از نسخه‌ی اون یک [فورک](https://gitlab.com/GNULand/TraktorPlus/tor-status) برای خودمون گرفتیم و تغییراتی رو که نیاز داشتیم روی نسخه‌ی خودمون اعمال کردیم و در بروزرسانی ۲.۳ تراکتور گذاشتیم.

بطور کل این نگارش چندین تغییر کوچیک بزرگ داشت که چهار تا از اصلی‌ترین تغیرات شامل موارد زیر میشه:  
۱. بازنویسی پنل تراکتور  
۲. عوض کردن آیکون‌های پنل تراکتور  
۳. حل مشکل پرونده torrc که بعد از بروزرسانی تور، تغییر می‌کرد به نسخه‌ی پیش‌فرض خود پروژه تور و باعث میشد اتصال قطع بشه\(بخاطر فیلترینگ\)  
۴. عوض کردن آدرس libssl

تصویر پنل تراکتور:  
![](traktor_v2-3)بطور کامل مشکلات قبلی حل شده و کاملا آماده‌ی استفادست.  
ولی یه توضیح در مورد کار کردش بدم بد نیست؛  
ببینید برای اجراش، باید از دش Traktor Panel رو جستجو کنید و بزنید که باز بشه. بعد میره اون بالا\(شایدم پایین، بستگی به میزکار داره\).  
بعدی که باز شد بالا وضعیت تور رو میگه، این که وصل هست یا قطع و یا ... . میتونید سرویس تور رو از همینجا راه‌اندازی مجدد کنید و پروکسی رو فعال و غیر‌فعال کنید.  
\(نکته‌ی مهمی که نباید فراموش کنید اینه که تراکتور پنل برای بررسی اتصالش به اینترنت، به آدرس https://3g2upl4pq6kufc4m.onion/ که hidden service موتور‌جستجوی داک‌داک‌گو میشه درخواستی بطور رمزنگاری شده\(socks5 و SSL/TLS\) میفرسته تا ناشناسیتون رو تضمین کنه\)

در نگارش‌های بعدی تلاش می‌کنم برای گنوم، اکستنشن رو هم اضافه کنم تا همه راحت باشن.

موفق باشید...




# نصب اوبونتو ۱۷.۱۰

این مطلب رو قبلا در وبلاگم منتشر کرده بودم، اما در اینجا یک بازنویسی انجام دادم و سعی کردم مراحل نصب رو بهتر توضیح بدم.  
نصب اوبونتو ۱۷.۱۰ نسبت به نگارش‌های قبلی، یکم عوض شده که در این مطلب توضیحش دادم.



**اوبونتو چیست؟  
**اوبونتو \(به انگلیسی Ubuntu\) \(تلفّظ: oo-boon-too\) یک توزیع از سیستم‌عاملی به نام گنو/لینوکس است که توسّط جامعهٔ کاربری‌اش برای بن‌سازه‌های مختلفی چون رایانه‌های شخصی، کارسازها، تلفن‌های همراه، رایانک‌ها، تلویزیون‌ها، اینترنت چیزها و… توسعه داده می‌شود و از حمایت رسمی شرکت کنونیکال برخوردار است. این توزیع در راستای آسان کردن استفاده از سیستم‌عامل گنو/لینوکس و نرم‌افزار آزاد برای همهٔ اقشار جامعه تلاش می‌کند.  
برای اطلاعات بیشتر در مورد اوبونتو [اینجا](https://wiki.ubuntu.ir/wiki/%D8%A7%D9%88%D8%A8%D9%88%D9%86%D8%AA%D9%88) کلیک کنید.

**همین اول کار چندین نکته رو بگم که توی نصب دچار مشکل نشید:**  
۱. عکسا مربوط میشه به نگارش ۱۶.۰۴. ولی نگران نباشید مراحل نصب و آموزش نگارش ۱۷.۱۰ با ۱۶.۰۴ یکی هستش.  
۲. این مطلب هم برای نصب به جای ویندوز هست و هم در کنار ویندوز، لذا همون بخش مورد نظر خودتون رو توی مطلب پیدا کنید و بخونید.  
۳. نصب کنار ویندوز دو حالت داره. یه حالت اینه که گزینه‌ای رو بزنید که خودکار خودش اوبونتو رو به جای ویندوز نصب می‌کنه و حالت دیگه شباهت بسیاری به مراحل نصب به جای ویندوز داره. پس اگر دیدن حالت دوم با مراحل نصب به جای ویندوز شباهت داره فکر نکنید سوتی دادم :\)  
۴. از نگارش ۱۷.۱۰ به بعد، برای اوبونتو نیاز به تعریف پارتیشن سواپ نیست و خود سیستم‌عامل موقع نیاز بخشی از فضای هاردتون رو استفاده می‌کنه.  
۵. از Basic بودن هارد خودتون مطمئن باشید، اگر نباشید ممکنه دچار مشکل بشید. توی طول آموزش گفتم چجوری بررسی کنید و اگر Basic نبود چجوری مشکل رو حل کنید.



**راهنمای نصب: \(نصب به جای ویندوز\)  
**

**قدم اول:**

برای نصب اوبونتو ۱۷.۱۰، ابتدا باید اون رو از سایت خود اوبونتو بارگیری کنیم. برای بارگیری [اینجا](https://www.ubuntu.com/download/desktop) کلیک کنید.

---

**قدم دوم:**

پرونده‌ی iso بارگیری شده رو باید یا روی یک فلش حداقل ۴GB بوتیبل کنید و یا بر روی یک DVD رایت کنید.  
اگر نیاز به آموزش دارید، بر روی هر کدوم از پیوند‌های پایین که خواستید کلیک کنید:

**برای اوبونتو:**  
۱. رایت کردن فایل بر روی دی‌وی‌دی: \([How to burn a DVD on Ubuntu](https://tutorials.ubuntu.com/tutorial/tutorial-burn-a-dvd-on-ubuntu#0)\)  
۲. بوتیبل کردن فایل روی فلش: \([How to create a bootable USB stick on Ubuntu](https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-ubuntu#0)\)  
۳. اگر ۱۷.۰۴ هستید و قصد دارید به ۱۷.۱۰ ارتقا بدید اینجا کلیک کنید.

**برای مک:**  
۱. رایت کردن فایل بر روی دی‌وی‌دی: \([How to burn a DVD on macOS](https://tutorials.ubuntu.com/tutorial/tutorial-burn-a-dvd-on-macos#0)\)  
۲. بوتیبل کردن فایل روی فلش: \([How to create a bootable USB stick on macOS](https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-macos#0)\)

\* _**نکته**:_  
_اوبونتو قابلیت اجرا بصورت زنده یا Live را دارد\(بدون نصب\)، کافیه از طریق فلش یا DVD اون رو بر روی سیستمتون اجرا کنید._

---

**قدم سوم:  
**

بعد از انجام بوتیبل و یا رایت کردن بر روی DVD، سیستم رو راه‌اندازی مجدد\(restart\) کنید و از BIOS، بوت سیستم رو روی فلش یا DVD بذارید.

\* _**نکته**_:  
_اگر هاردتون GPT هست بهتره اوبونتو رو UEFI بوت کنید._

اگر درست پیش برید، باید به صفحه زیر برخورد کنید:![](grub)گزینه اول برای امتحان کردن اوبونتو به صورت زنده و بدون نیاز به نصب هست.  
گزینه دوم هم برای نصب اوبونتو.

\* _**نکته**:_  
_اگر گزینه اول رو انتخاب کنید، میتونید از داخل همون محیط اقدام به نصب اوبونتو کنید._

---

**قدم چهارم:**

حالا باید با این صفحه رو به رو بشید:  
![](/images/install-ubuntu1710/ubuntu-loading.jpg)  
\* _**نکته**:_  
_از این مرحله به بعد من برای این که بتونم عکس بگیرم، مجبور شدم گزینه‌ی اجرای زنده‌ی اوبونتو بزنم و از اونجا اقدام به نصب کنم. ولی نگران نباشید، هر دو گزینه برای نصب عین هم هستن و فرقی ندارن :\)_

حالا باید صفحه زیر رو ببینید:  
![](/images/install-ubuntu1710/live-installation.jpg)روی گزینه Continue کلیک کنید.

---

**قدم پنجم:**

![](/images/install-ubuntu1710/live-installation2.jpg)برای فعال شدن این دو گزینه باید اینترنتتون وصل باشه، اگه وصل نباشه این دو گزینه غیر فعال میشن.

گزینه اول برای نصب بروزرسانیا و اینطور چیزاست.  
گزینه دوم برای نصب کدک‌های تصویری و صوتی هست.

اگر تیکشون نزنید، بعدا میتونید از توی خود اوبونتو فعالشون کنید.

هر دو تیک رو فعال کنید و روی گزینه Continue بزنید.

---

**قدم ششم:**

![](/images/install-ubuntu1710/ubuntu-installation-type.jpg)گزینه اول برای نصب کنار ویندوز هست.  
دومی حذف اوبونتو فعلی و نصب اوبونتو جدید.  
گزینه سوم و جهارم رو کاری نداریم. حالت‌های نصب دیگست که منم بلدشون نیستم...  
اما گزینه چهارم، اینجا میتونید پارتیشن بندی دلخواهتون رو بکنید.

دقیقا یادم نیست گزینه دوم چیا رو پاک می‌کنه، آيا فقط شاخه روت رو پاک می‌کنه و یا کل اوبونتو\(شاخه‌ی خونگی و روت\). در نتیجه اجازه بدید با گزینه آخر یعنی Something else بریم جلو که خیالمون راحت باشه.

روی گزینه Something else بزنید و روی Install Now کلیک کنید.

---

**قدم هفتم:**

![](/images/install-ubuntu1710/ubuntu-installation-type2.jpg)توی این مرحله بطور دستی میشه هارد رو برای نصب اوبونتو پارتیشن بندی کرد.

اینجا هر تغییری بخواید رو میتونید بدید، اگر می‌خواد اوبونتو رو به جای ویندوز نصب کنید، کافیه روی پارتیشن مربوط به ویندوزتون\(همون پارتیشنی که روش ویندوز نصبه\) کلیک کنید و روی گزینه - بزنید تا اون حذف بشه.

\* _**نکته**:_  
۱. _هر تغییری توی این محیط انجام دادید و به مشکل بر خوردید کافیه دکمه‌ی Revert رو بزنید. تا تنظیمات برگرده به حالت اول.  
۲. اگر احساس می‌کنید مثل عکس زیر پارتیشناتون رو درست نشون نمیده، _[_اینجا_](https://soshaw.net/ubuntu-installing-problem-solving/)_ کلیک کنید.  
_![](/images/install-ubuntu1710/ubuntu-installation-type3.jpg)_  
_

مثلا اینجا برای من ویندوز روی sda1 هست، منم روش میتونم کلیک کنم و گزینه - رو بزنم تا حجمش اضافه بشه به free space.  
شمام همچین کاری رو باید برای خودتون بکنید.

ما باید یک پارتیشن root و یه پارتیشن home تعریف کنیم.  
حالا روی گزینه free space کلیک می‌کنیم و روی گزینه + میزنیم:  
![](/images/install-ubuntu1710/ubuntu-installation-type4.jpg)ابتدا root:  
Size مربوط به حجمی که می‌خواید اختصاص بدید داره. تقریبا ۲۰ گیگ کافیه، تازه خیلیم زیاده اما دیگه خیالتون راحته.  
Type for the new partition رو بذارید روی Primary. اگر هارتون MBR هست و اجازه نمیده پارتیشن Primary بسازید، میتونید این گزینه رو بذارید روی Logical.  
Location for the new partition رو روی هر کدوم خواستید میتونید بذارید، پیشنهاد من Beginning of this space هست.  
Use as رو بذارید روی Ext4 journaling file system.  
Mount point رو هم بذارید روی / \(طبق عکس\).

حالا دوباره روی گزینه free space کلیک کنید و روی گزینه + بزنید:  
![](/images/install-ubuntu1710/ubuntu-installation-type5.jpg)برای home:  
Size مربوط به حجمی که می‌خواید اختصاص بدید داره. هر چقدر حجم اضافه مونده رو بدید به پارتیشن home.  
Type for the new partition رو بذارید روی Primary. اگر هارتون MBR هست و اجازه نمیده پارتیشن Primary بسازید، میتونید این گزینه رو بذارید روی Logical.  
Location for the new partition رو روی هر کدوم خواستید میتونید بذارید، پیشنهاد من Beginning of this space هست.  
Use as رو بذارید روی Ext4 journaling file system.  
Mount point رو هم بذارید روی /home \(طبق عکس\).

روی گزینه OK کلیک کنید و بعد بررسی کنید همه چی درست باشه. اگر همه چی درست بود روی گزینه Install Now کلیک کنید.

حالا باید با همچین چیزی برخورد کنید\(تقریبا البته!\):  
![](/images/install-ubuntu1710/ubuntu-installation-type6.jpg)باید دو خط آخر شماره پارتیشنایی که ساختید و قالبشون رو بگه، مثل عکس بالا.  
اگر همه چی درست بود Continue بزنید.

---

**قدم هشتم:**

![](/images/install-ubuntu1710/location.jpg)شما میتونید کشور ایران رو انتخاب کنید تا ساعت ایران برای سیستم تنظیم بشه. ولی من یه نکته رو باید بگم. وقتی شما کشور ایران رو انتخاب می کنید، تقویم اوبونتو به فارسی میشه. نمایش ساعت فارسی میشه.  
ممکنه بعضیا دوست نداشته باشن. برای همین بهتره کشور رو همین New York که پیشفرضه انتخاب کنید. بعد از نصب میتونید ساعت رو روی ایران تنظیم کنید.

روی گزینه Continue بزنید.

---

**قدم نهم:**

![](/images/install-ubuntu1710/keyboard-layout.png)اگه کشور رو نیویورک انتخاب کنید، این قسمت زبان به صورت پیشفرض روی English US هست. ولی اگه کشور رو ایران انتخاب کردید، حتما زبان رو از روی فارسی بردارید و همین English US رو انتخاب کنید. چون موقع نوشتن گذرواژه، میخواد فارسی بنویسه و ممکنه دچار مشکل بشید. پس زبان رو انگلیسی انتخاب کنید.  
اونجایی که نوشته Type here to test yor keyboard شما میتونید امتحان کنید که صفحه‌کلیدتون کار می‌کنه یا نه. یا ببینید زبان مورد نظرتون رو به درستی نوشته میشه یا نه.

روی گزینه Continue بزنید.

---

**قدم دهم:**

![](/images/install-ubuntu1710/information.jpg)Your Name رو وارد می کنید \(به دلخواه\). در حین نوشتن، فیلد Your Computer و Username هم تکمیل میشه. اگه خواستید میتونید اونارو به ترتیب ویرایش کنید.  
Your Computer's name رو وارد کنید. یادتون باشه که این فیلد نباید حاوی کلمات فاصله‌دار باشه.  
pick a username رو وارد می کنید. برای امنیت بالاتر، بهتره گذرواژه طولانی‌تری انتخاب کنید.

اون سه تا جمله‌ی پایینی رو یه توضیح بدم:  
جمله‌ی اول یعنی بدون اینکه ازتون گذرواژه بخواد وارد سیستم بشه. که خب مسلما هیچکس چنین چیزی رو دوست نداره. پس بذارین همین جمله‌ی دومی تیک داشته باشه. \(مگه اینکه سیستم شما سیستمی عمومی باشه. که در این صورت اصلا نیازی به گذرواژه گذاشتن نیست\).  
جمله ی سوم هم میگه که پوشه‌ی هوم شما رمزنگاری میشه و هر کسی مجوز ورود به هوم رو پیدا نمی کنه.\(پیشنهاد می‌کنم نزنید چون اگر بعدا اشتباه کنید کل محتوای توشو از دست میدید\)

روی گزینه Continue بزنید.

---

**مرحله یازدهم:**

![](/images/install-ubuntu1710/install.jpg)الان وارد مرحله نصب شدید.  
با زدن روی گزینه Installing system میتونید جزییات بیشتری ببینید.

عکس‌هایی از مراحل نصب:  
![](/images/install-ubuntu1710/install2.jpg)
![](/images/install-ubuntu1710/install3.jpg)
![](/images/install-ubuntu1710/install4.jpg)صبر کنید تا این مرحله تمام بشه.  
چیزی باید حدود ۵تا ۲۰ دقیقه طول بده. البته ربط مستقیمی به سرعت سیستم و سرعت اینترنتتون\(در صورت متصل بودن\) داره.

---

**مرحله دوازدهم:**

![](/images/install-ubuntu1710/finish-install.jpg)
اگر پیغام بالا رو بگیرید یعنی نصب با موقیت به پایان رسیده و کافیه روی گزینه Restart Now کلیک کنید تا سیستم راه‌اندازی مجدد بشه.

---

**قدم سیزدهم:**

![](/images/install-ubuntu1710/bootloader-grub.jpg)حالا باید همچین صفحه‌ای رو ببینید. به این میگن گراب.  
حالا گزینه Ubuntu یعنی همین گزینه اول رو Enter کنید تا بریم وارد محیط اوبونتو بشیم...

اونجا بعدا نام‌کاربریتون رو نوشته و شما میتونید روش کلیک کنید، گذرواژه‌تون رو بنویسید و وارد محیط اوبونتو بشید. \(یه چیز تو مایه‌های عکسای زیر، اما این عکسا مربوط به خدا بیامرز یونیتی و نگارش ۱۶.۰۴ هست\)  
![](/images/install-ubuntu1710/login-screen.jpg)  
همه چی تمومه و ورود شما رو به جامعه کاربری اوبونتو تبریک میگم! :\)



**راهنمای نصب: \(نصب در کنار ویندوز\)**

قبل از شروع خوبه نکته‌ای رو بگم:  
آموزش ابتدا با خود ویندوز شروع میشه و کارایی که توی ویندوز باید بکنید و بعد میرسه به نصب اوبونتو. در موقع آموزش نصب اوبونتو، آموزش به دو دسته تقسیم میشه. اونجا هر کدوم رو که خواستید دنبال کنید و نیاز نیست هر دو مرحله رو باهم برید :\)

**قدم اول:**

ابتدا باید مقداری از فضای هاردتون رو اختصاص بدید برای نصب اوبونتو. یعنی باید مقداری از حافظتون خالی باشه و یا خالی کنید براش. من ۵۰گیگ رو برای شروع ایده‌ه‌آل میدونم. پس شمام سعی کنید حداقل ۵۰گیگ جدا کنید.

برای این کار میتونید مقداری از حجم یه پارتیشنتون رو خالی کنید و بعد، از پارتیشن اصلی جداش کنید که اصطلاحا بهش میگن shrink.  
ابتدا به صفحه دسکتاپ ویندوز خودتون برید:
![](/images/install-ubuntu1710/Amoozesh-1.jpg)  
بعد روی گزینه This PC\(که خیلی هم اسمش به کارش بی ربطه\) کلیک راست کنید و روی گزینه Manage کلیک کنید:  
![](/images/install-ubuntu1710/Amoozesh-2.jpg)  
صفحه‌ای به اسم Computer Management باز میشه که شما باید روی بخش گزینه‌ی Disk Management که در سمت چپ تصویر پیداست کلیک کنید:  
\* _**نکته**_:  
_اگر برای شما در تصویر پایین در قسمت Type\(که برای من نوشته Basic\) برای شما Dynamic نوشته بود، باید اول این آموزش رو دنبال کنید و بعد برگردید به این مطلب برای ادامه‌ی نصب._  
\* _**نکته**_:  
_اگر از ویندوز ۱۰ استفاده می‌کنید،‌ احتمال این که نتوانید Disk Dynamic رو به Dynamic Basic تبدیل کنید بسیار بالاست، در نتیجه نمی توانید هیچ سیستم عاملی جز خود ویندوز نصب کنید. \(اگر از بروزرسانی جدید ویندوز ۱۰ استفاده می کنید،‌ احتمال بروز این مشکل نزدیک به ۱۰۰٪ هستش!\)‌. تنها راه حل این مشکل، پارتیشن‌بندی کردن هارد، از اول است.  
اما شاید بد نباشه به _[_این_](https://soshaw.net/how-to-convert-dynamic-disk-to-basic-disk/)_ آموزش که دوستم چندین ماه پیش بهم داد و من توی وبلاگم منتشر کردم یه نگاهی بندازید:_  
![](/images/install-ubuntu1710/Amoozesh-3.jpg)
در قسمت Disk Management تعداد پارتیشن‌ها و فضای کل هارد دیسک را مشاهده می‌کنید.

بر روی پارتیشنی که قصد دارید حجمی ازش کم کنید برید و کلیک راست کنید و روی گزینه‌ی Shrink Volume کلیک کنید.  
باید با همچین صفحه ای رو به رو بشید:  
![](/images/install-ubuntu1710/Amoozesh-4.jpg)
حجم کل پارتیشن رو گفته: 199649MB  
حجم مقدار خالی برای استفاده: 156091MB  
حجمی که قصد داریم کم کنیم:\(که من 50GB انتخاب کردم\)  
حجم در دسترس بعد از Shrink کردن: 149649MB  
خب گزینه Shrink رو میزنیم.

![](/images/install-ubuntu1710/Amoozesh-5.jpg)
بعد از زدن گزینه shrink شاهد کم شدن حجم پارتیشن قبلی و اضافه شدن نوشته‌ی Unallocated میشیم.

---

**قدم دوم:**

برای نصب اوبونتو ۱۷.۱۰، ابتدا باید اون رو از سایت خود اوبونتو بارگیری کنیم. برای بارگیری [اینجا](https://www.ubuntu.com/download/desktop) کلیک کنید.

---

**قدم سوم:**

پرونده‌ی iso بارگیری شده رو باید یا روی یک فلش حداقل ۴GB بوتیبل کنید و یا بر روی یک DVD رایت کنید.  
اگر نیاز به آموزش دارید، بر روی هر کدوم از پیوند‌های پایین که خواستید کلیک کنید:

**برای ویندوز:**  
۱. رایت کردن فایل بر روی دی‌وی‌دی: \([How to burn a DVD on Windows](https://tutorials.ubuntu.com/tutorial/tutorial-burn-a-dvd-on-windows#0)\)  
۲. بوتیبل کردن فایل روی فلش: \([How to create a bootable USB stick on Windows](https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-windows#0)\)

\* _**نکته**:_  
_اوبونتو قابلیت اجرا بصورت زنده یا Live را دارد\(بدون نصب\)، کافیه از طریق فلش یا DVD اون رو بر روی سیستمتون اجرا کنید._

---

**قدم چهارم:  
**

بعد از انجام بوتیبل و یا رایت کردن بر روی DVD، سیستم رو راه‌اندازی مجدد\(restart\) کنید و از BIOS، بوت سیستم رو روی فلش یا DVD بذارید.

\* _**نکته**_:  
_اگر هاردتون GPT هست بهتره اوبونتو رو UEFI بوت کنید.  
اگر ویندوزتون UEFI نصب شده، باید اوبونتو رو هم به صورت UEFI بوت و نصب کنید.  
_

اگر درست پیش برید، باید به صفحه زیر برخورد کنید:
![](/images/install-ubuntu1710/grub.jpg)
گزینه اول برای امتحان کردن اوبونتو به صورت زنده و بدون نیاز به نصب هست.  
گزینه دوم هم برای نصب اوبونتو.

\* _**نکته**:_  
_اگر گزینه اول رو انتخاب کنید، میتونید از داخل همون محیط اقدام به نصب اوبونتو کنید._

---

**قدم پنجم:**

حالا باید با این صفحه رو به رو بشید:  
![](/images/install-ubuntu1710/ubuntu-loading.jpg)  
\* _**نکته**:_  
_از این مرحله به بعد من برای این که بتونم عکس بگیرم، مجبور شدم گزینه‌ی اجرای زنده‌ی اوبونتو بزنم و از اونجا اقدام به نصب کنم. ولی نگران نباشید، هر دو گزینه برای نصب عین هم هستن و فرقی ندارن :\)_

حالا باید صفحه زیر رو ببینید:  
![](/images/install-ubuntu1710/live-installation.jpg)
روی گزینه Continue کلیک کنید.

---

**قدم ششم:**

![](/images/install-ubuntu1710/live-installation2.jpg)برای فعال شدن این دو گزینه باید اینترنتتون وصل باشه، اگه وصل نباشه این دو گزینه غیر فعال میشن.

گزینه اول برای نصب بروزرسانیا و اینطور چیزاست.  
گزینه دوم برای نصب کدک‌های تصویری و صوتی هست.

اگر تیکشون نزنید، بعدا میتونید از توی خود اوبونتو فعالشون کنید.

هر دو تیک رو فعال کنید و روی گزینه Continue بزنید.

---

**قدم هفتم:**

![](/images/install-ubuntu1710/ubuntu-installation-type.jpg)
گزینه اول برای نصب کنار ویندوز هست.  
دومی حذف اوبونتو فعلی و نصب اوبونتو جدید.  
گزینه سوم و جهارم رو کاری نداریم. حالت‌های نصب دیگست که منم بلدشون نیستم...  
اما گزینه چهارم، اینجا میتونید پارتیشن بندی دلخواهتون رو بکنید.

از این مرحله به بعد، دو راه دارید برای نصب، یکی بزنید خودکار اوبونتو رو کنار ویندوز نصب کنه، یا دستی خودتون پارتیشن‌بندی کنید.  
اگر بزنید که خودکار انجام بده، خب خودش پارتیشنی رو که خالی کردید و یا فضای خالی رو تشخیص میده و میره روش اوبونتو نصب می‌کنه.\(من زیاد اینطوری دوست ندارم\)  
اگرم بزنید که دستی انجام بدید، خب طبق آموزشی که پایین‌تر هست میتونید پیش برید.\(این رو توصیه می‌کنم\)

نکته‌ای که اینجاست اینه که **قدم هشتم** در پایین برای کسایی که می‌خوان **دستی** برن جلو مورد استفاده **نباید** قرار بگیره و باید این افراد برن سراغ **قدم نهم**. اما کسایی که می‌خوان خودکار انجام بشه، باید **قدم هشتم** رو دنبال کنن.

---

**قدم هشتم:**\(مخصوص کسانی که گزینه خودکار رو انتخاب کردن\)

در این قسمت، با انتخاب گزینه‌ی Install Ubuntu alongside Windows \(7,8,10\)\(نصب اوبونتو \[به صورت خودکار\] در کنار ویندوز\) عکس زیر رو باید نشون بده تقریبا:

\* _**نکته**_:  
ا_ینجا کل اون ۵۰GB یا هر چی که شما جدا کردید رو اختصاص میده به شاخه روت، این کار باعث میشه بعدا اگر اوبونتو رو بخواید بلایی سرش بیارید پرونده‌های توشم بپره. سر همین پیشنهاد داده بودم که دستی برید جلو. چون اونجا میتونستید یه شاخه خونگی هم تعریف کنید و در این صورت پرونده‌هاتون رو میفرستادید توی اون شاخه..._

![](/images/install-ubuntu1710/ubuntu-installation-type2.jpg)
اینجا در انتهای نوشته‌ها، باید شماره پارتیشنایی که ساخته و قالبشون رو بگه، مثل عکس بالا. \(البته فکر می‌کنم swap رو نسازه چون همونطور که گفتیم توی نسخه ۱۷.۱۰ سواپ دیگه نیاز به پارتیشن جدا نداره\)  
اگر همه چی درست بود Continue بزنید.

از اینجا به بعد ادامه آموزش رو دنبال کنید، بقیه مراحل مثل نصب دستی هست. فقط همین یه تیکش فرق داشت :\)

---

**قدم نهم:**

![](/images/install-ubuntu1710/ubuntu-installation-type3.jpg)
توی این مرحله بطور دستی میشه هارد رو برای نصب اوبونتو پارتیشن بندی کرد.

اینجا هر تغییری بخواید رو میتونید بدید، اگر می‌خواد اوبونتو رو به جای ویندوز نصب کنید، کافیه روی پارتیشن مربوط به ویندوزتون\(همون پارتیشنی که روش ویندوز نصبه\) کلیک کنید و روی گزینه - بزنید تا اون حذف بشه.

\* _**نکته**:_  
_هر تغییری توی این محیط انجام دادید و به مشکل بر خوردید کافیه دکمه‌ی Revert رو بزنید. تا تنظیمات برگرده به حالت اول._

مثلا اینجا برای من ویندوز روی sda1 هست، منم روش میتونم کلیک کنم و گزینه - رو بزنم تا حجمش اضافه بشه به free space.  
شمام همچین کاری رو باید برای خودتون بکنید.

ما باید یک پارتیشن root و یه پارتیشن home تعریف کنیم.  
حالا روی گزینه free space کلیک می‌کنیم و روی گزینه + میزنیم:  
![](/images/install-ubuntu1710/ubuntu-installation-type4.jpg)
ابتدا root:  
Size مربوط به حجمی که می‌خواید اختصاص بدید داره. تقریبا ۲۰ گیگ کافیه، تازه خیلیم زیاده اما دیگه خیالتون راحته.  
Type for the new partition رو بذارید روی Primary. اگر هارتون MBR هست و اجازه نمیده پارتیشن Primary بسازید، میتونید این گزینه رو بذارید روی Logical.  
Location for the new partition رو روی هر کدوم خواستید میتونید بذارید، پیشنهاد من Beginning of this space هست.  
Use as رو بذارید روی Ext4 journaling file system.  
Mount point رو هم بذارید روی / \(طبق عکس\).

حالا دوباره روی گزینه free space کلیک کنید و روی گزینه + بزنید:  
![](/images/install-ubuntu1710/ubuntu-installation-type5.jpg)
برای home:  
Size مربوط به حجمی که می‌خواید اختصاص بدید داره. هر چقدر حجم اضافه مونده رو بدید به پارتیشن home.  
Type for the new partition رو بذارید روی Primary. اگر هارتون MBR هست و اجازه نمیده پارتیشن Primary بسازید، میتونید این گزینه رو بذارید روی Logical.  
Location for the new partition رو روی هر کدوم خواستید میتونید بذارید، پیشنهاد من Beginning of this space هست.  
Use as رو بذارید روی Ext4 journaling file system.  
Mount point رو هم بذارید روی /home \(طبق عکس\).

روی گزینه OK کلیک کنید و بعد بررسی کنید همه چی درست باشه. اگر همه چی درست بود روی گزینه Install Now کلیک کنید.

حالا باید با همچین چیزی برخورد کنید\(تقریبا البته!\):  
![](/images/install-ubuntu1710/ubuntu-installation-type6.jpg)
باید دو خط آخر شماره پارتیشنایی که ساختید و قالبشون رو بگه، مثل عکس بالا.  
اگر همه چی درست بود Continue بزنید.

---

**قدم دهم:**

![](/images/install-ubuntu1710/location.jpg)
شما میتونید کشور ایران رو انتخاب کنید تا ساعت ایران برای سیستم تنظیم بشه. ولی من یه نکته رو باید بگم. وقتی شما کشور ایران رو انتخاب می کنید، تقویم اوبونتو به فارسی میشه. نمایش ساعت فارسی میشه.  
ممکنه بعضیا دوست نداشته باشن. برای همین بهتره کشور رو همین New York که پیشفرضه انتخاب کنید. بعد از نصب میتونید ساعت رو روی ایران تنظیم کنید.

روی گزینه Continue بزنید.

---

**قدم یازدهم:**

![](/images/install-ubuntu1710/keyboard-layout.png)
اگه کشور رو نیویورک انتخاب کنید، این قسمت زبان به صورت پیشفرض روی English US هست. ولی اگه کشور رو ایران انتخاب کردید، حتما زبان رو از روی فارسی بردارید و همین English US رو انتخاب کنید. چون موقع نوشتن گذرواژه، میخواد فارسی بنویسه و ممکنه دچار مشکل بشید. پس زبان رو انگلیسی انتخاب کنید.  
اونجایی که نوشته Type here to test yor keyboard شما میتونید امتحان کنید که صفحه‌کلیدتون کار می‌کنه یا نه. یا ببینید زبان مورد نظرتون رو به درستی نوشته میشه یا نه.

روی گزینه Continue بزنید.

---

**قدم دوازدهم:**

![](/images/install-ubuntu1710/information.jpg)
Your Name رو وارد می کنید \(به دلخواه\). در حین نوشتن، فیلد Your Computer و Username هم تکمیل میشه. اگه خواستید میتونید اونارو به ترتیب ویرایش کنید.  
Your Computer's name رو وارد کنید. یادتون باشه که این فیلد نباید حاوی کلمات فاصله‌دار باشه.  
pick a username رو وارد می کنید. برای امنیت بالاتر، بهتره گذرواژه طولانی‌تری انتخاب کنید.

اون سه تا جمله‌ی پایینی رو یه توضیح بدم:  
جمله‌ی اول یعنی بدون اینکه ازتون گذرواژه بخواد وارد سیستم بشه. که خب مسلما هیچکس چنین چیزی رو دوست نداره. پس بذارین همین جمله‌ی دومی تیک داشته باشه. \(مگه اینکه سیستم شما سیستمی عمومی باشه. که در این صورت اصلا نیازی به گذرواژه گذاشتن نیست\).  
جمله ی سوم هم میگه که پوشه‌ی هوم شما رمزنگاری میشه و هر کسی مجوز ورود به هوم رو پیدا نمی کنه.\(پیشنهاد می‌کنم نزنید چون اگر بعدا اشتباه کنید کل محتوای توشو از دست میدید\)

روی گزینه Continue بزنید.

---

**مرحله سیزدهم:**

![](/images/install-ubuntu1710/install.jpg)
الان وارد مرحله نصب شدید.  
با زدن روی گزینه Installing system میتونید جزییات بیشتری ببینید.

عکس‌هایی از مراحل نصب:  
![](/images/install-ubuntu1710/install2.jpg)
![](/images/install-ubuntu1710/install3.jpg)
![](/images/install-ubuntu1710/install4.jpg)
صبر کنید تا این مرحله تمام بشه.  
چیزی باید حدود ۵تا ۲۰ دقیقه طول بده. البته ربط مستقیمی به سرعت سیستم و سرعت اینترنتتون\(در صورت متصل بودن\) داره.

---

**مرحله چهاردهم:**

![](/images/install-ubuntu1710/finish-install.jpg)
اگر پیغام بالا رو بگیرید یعنی نصب با موقیت به پایان رسیده و کافیه روی گزینه Restart Now کلیک کنید تا سیستم راه‌اندازی مجدد بشه.

---

**قدم پانزدهم:**

![](/images/install-ubuntu1710/bootloader-grub.jpg)
حالا باید همچین صفحه‌ای رو ببینید. به این میگن گراب.  
حالا گزینه Ubuntu یعنی همین گزینه اول رو Enter کنید تا بریم وارد محیط اوبونتو بشیم...

اونجا بعدا نام‌کاربریتون رو نوشته و شما میتونید روش کلیک کنید، گذرواژه‌تون رو بنویسید و وارد محیط اوبونتو بشید. \(یه چیز تو مایه‌های عکسای زیر، اما این عکسا مربوط به خدا بیامرز یونیتی و نگارش ۱۶.۰۴ هست\)  
![](/images/install-ubuntu1710/login-screen.jpg)

همه چی تمومه و ورود شما رو به جامعه کاربری اوبونتو تبریک میگم! 🙂



احتمالا بعد از نصب چند مشکل کوچیک رو داشته باشید، مثلا موقع روشن کردن سیستم توی گراب ویندوز نباشه.  
خب دو حالت داره، یا ویندوز رو پروندید یا هستش و باید به گراب اضافش کنید که راه‌حلش [اینه](https://soshaw.net/grub-problem/).  
یا ممکنه وارد اوبونتو نتونید بشید که راهش خاموش کردن fast boot ویندوز هست.

در هر صورت هر مشکلی بود همینجا مطرح کنید، بتونیم پاسخ میدیم :\)



موفق باشید...




# آموزش نصب و راه‌اندازی افزونه‌های gedit
Video

![](/images/gedit/Gedit-logo-clean.png)

**جی‌ادیت** \(به انگلیسی: **gedit**\) ویرایشگر متن در میزکار گنوم است که در گنو/لینوکس، ویندوز و مک او اس کار می‌کند. طراحی آن به صورت یک ویرایشگر متن چندمنظوره است و همچنین از ابزارهایی برای ویرایش کد منبع و زبان‌های ساختاری همچون زبان نشانه‌گذاری بهره می‌برد.  
این نرم‌افزار طوری طراحی شده‌است که رابط کاربری ساده و تمیزی داشته‌باشد و تحت پروانه عمومی همگانی گنو منتشر می‌شود.  
این برنامه بصورت پیشفرض در بسیاری از توزیع‌های اوبونتو نصب می‌باشد.  
حالا ما برای جذاب‌تر شدن این ویرایشگر برای کد زدن، تعدادی افزونه و نصبشون رو در فیلم به شما آموزش میدیم.

**دستورات مورد استفاده در فیلم:**  
برای نصب:

```
$ sudo apt install gedit
```

لیست افزونه‌ها و سایر برنامه‌های gedit:

```
gedit-common                      gedit-plugin-find-in-files
gedit-dev                         gedit-plugin-git
gedit-developer-plugins           gedit-plugin-join-lines
gedit-latex-plugin                gedit-plugin-multi-edit
gedit-plugin-bookmarks            gedit-plugins
gedit-plugin-bracket-completion   gedit-plugins-common
gedit-plugin-character-map        gedit-plugin-smart-spaces
gedit-plugin-code-comment         gedit-plugin-synctex
gedit-plugin-color-picker         gedit-plugin-terminal
gedit-plugin-color-schemer        gedit-plugin-text-size
gedit-plugin-commander            gedit-plugin-word-completion
gedit-plugin-dashboard            gedit-plugin-zeitgeist
gedit-plugin-draw-spaces          gedit-source-code-browser-plugin
```

\* منابع: [ویکی‌پدیا](https://fa.wikipedia.org/wiki/%D8%AC%DB%8C%E2%80%8C%D8%A7%D8%AF%DB%8C%D8%AA)




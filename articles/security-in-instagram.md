# امنیت در اینستاگرام

افراد به اصطلاح هکر\(بچه‌اسکریپتی هایی با دانش بسیار پایین که همیشه هم از طرف مردم هم از سمت هکر ها جایگاه و مقبولیتی ندارن\)حساب های اینستاگرامی زیادی رو مورد هجوم قرار میدن و در نهایت اون اکانت هارو برای خودشون میگیرن و شروع به افشاگری میکنن یا اونهایی که دنبال کننده های زیادی دارن رو میفروشن. که البته این کار علاوه بر غیر‌اخلاقی بودن جرم هم حساب میشه.

این بچه اسکریپتی ها چند روش مرسوم برای عملی کردن اهدافشون و نفوذ به حساب‌های ما دارن:  
۱- ارسال گذرواژه توسط کاربر برای نفوذگر:  
خیلی از نفوذگر ها کاربران رو جوری گول می‌زنن که کابران به اونا اعتماد می‌کنن و خودشون گذرواژه رو براشون می‌فرستن\(حالا یا به روش های مستقیم یا در اسکرین‌شات یا ...\) و نفوذگر اون گذرواژه رو میگیره و صاحب اکانت میشه.این ساده‌ترین روش ممکنه و احتمالا با خودتون میگید کی همچین حماقتی میکنه؟باید بگم با اینکه این روش خیلی سادست اما خیلی هارو گول میزنه چون این افراد برای گرفتن رمز شما بهانه‌های خیلی جالبی میارن که اغلب وسوسه میشید!

> فیشینگ یکی از راه‌های هست که توسط سودجویان زیاد مورد استفاده قرار میگیره.

۲- روش هایی که به اسم Phishing معروف هستن. یعنی شمارو در دام خود می‌اندازند:  
در این روش یک صفحه ورود به حساب اینستاگرامی به شما داده میشه مثل instagram.com/accounts/login البته با کمی اختلاف مثلا: lnstagram \(متوجه تفاوت شدید؟ :دی\) که شمارو وادار به ورود به حساب خود می‌کنن و وقتی نام‌کاربری\(UserName\) و گذرواژه\(Password\) رو وارد کردین، اونا برای سازنده نرم‌افزار ارسال میشن. برای مثال میشه به نرم‌افزار هایی با عناوین "فالووربگیر" یا "لایک بگیر" اشاره کرد که UserName و Password رو میگیرند.

۳- گذرواژه های ساده و قابل حدس :  
یکی از رایج ترین روش ها که "brute force" نامیده میشه که به حدس گذرواژه توسط کِرَکِر\(کسی که عمل کرک را انجام می‌دهد\) میگن. یعنی گذرواژه شما طوریه که قابل حدس زدنه. چیز هایی مثل تاریخ تولد، اسم خودتون یا کسی که دوستش دارید، شماره کارت بانکی یا شماره تلفن و یا اسم حیوان خونگی یا .... کل کار اینجوری انجام میشه که کرکر طبق شناختی که از شما داره یک لیست از گذرواژه های مختلف که ممکنه شما از اون ها استفاده کنید رو آماده می‌کند و در نرم‌افزاری که برای اینکار طراحی شده وارد می‌کند و نرم‌افزار با سرعت بالا این گذرواژه هارو تست می‌کنه تا وقتی که وارد حساب شود یا لیست گذرواژه ها تموم بشه.  
همچنین گذرواژه های ساده مانند : 123454678 یا password و یا هر گذرواژه مرسوم دیگه ای معمولاً در لیست های گذرواژه آماده که به راحتی در اینترنت پیدا میشن وجود دارن و باعث میشه به راحتی گذرواژه شما لو بره پس از گذر واژه‌هایی قوی که ترکیبی از حروف،علائم و اعداد هستند استفاده کنید.

۴- بازیابی گذرواژه قسمت اول \(SMS\):  
در این روش نفوذگر با استفاده از هر راهی که تواناییش رو داره به SMS هایی که برای شماره شما ارسال میشه دسترسی داره و برای اینستاگرام از طرف شما یک درخواست برای بازیابی گذرواژه ارسال می‌کند و راه ‌SMS را انتخاب می‌کند و وقتی اینستاگرام برای حساب شما لینک برای بازیابی گذرواژه ارسال می‌کند، نفوذگر اونو میبینه و وارد حساب شما میشه و گذرواژه ای که خودش می‌خواهد را برای حساب انتخاب می‌کنه و…  
معمولاً در این روش نفوذگر به تلفن‌همراه شما دسترسی داره یا از برنامه‌ای دارای رفتارهای مخرب \(که قابلیت خواندن پیام‌های شما را داره\) استفاده میکنهو اونو به شما میده که اسم این برنامه‌ها RAT هست.  
نکته بسیار مهم:  
برای جلوگیری از این‌گونه حمله ها باید حواستون به نرم‌افزار هایی که نصب و اجرا می‌کنین باشه\(آنتی‌ویروس ها به هیچ وجه کارساز نیستند. خودتون باید حواستون باشه.\)

۵- بازیابی گذرواژه قسمت دوم\(Email\):  
در این روش هم نفوذگر به ایمیل شما دسترسی پیدا میکنه و وقتی به ایمیل دسترسی داشته باشه میتونه درخواست بازیابی گذرواژه رو از طریق ایمیل انتخاب کنه. البته دسترسی به ایمیل هم تنها با روش‌هایی که بالاتر گفتیم امکان‌پذیره پس تا زمانی که ایمیلتون امنه خیالتون از این قسمت راحت باشه!

۶- بازیابی گذرواژه قسمت سوم\(جعل تصویر\):  
در این روش \(که اخیراً بسیار مرسوم شده و مهم‌ترین قسمت ماجراست!\) نفوذگر برای نفوذ به حساب اینستاگرام شما روشی رو انتخاب می‌کنه که به ‌تازگی اضافه شده.در این روش نفوذگر به صفحه ورود اینستاگرام میره و نام‌کاربری شما را وارد میکنه و روی گزینه فراموشی رمز عبور کلیک میکنه و اینستاگرام ازش میخواد انتخاب کنه که کدوم روش رو برای بازیابی گذرواژش ترجیح میده؟\(بین دو گزینه شماره تلفن ویا ایمیل\) و نفوذگر گزینه سوم رو انتخاب میکنه\(!\) و ادعا میکنه که به هیچ کدوم از این دو گزینه دسترسی نداره. اینستاگرام از اون میخواد فرمی رو پر کنه و در اون جزئیات\(وغیره\) رو توضیح بده که مثلاً رمز ایمیلم رو فراموش کردم و خط تلفنم هم فروختم! و آدرس ایمیل فعلی خودش رو هم وارد میکنه و منتظر پاسخ اینستاگرام میمونه.  
اینستاگرام پاسخش رو درقالب یه ایمیل میده و بهش یک کد میده و ازش میخواد که کد رو روی کاغذ بنویسه و با اون عکس بگیره تا با مطابقت چهره‌اش با عکس‌های دیگش ثابت بشه حساب مال خودشه. در این لحظه نفوذگر یک عکس از قربانی رو با ابزار هایی مثل adobe photoshop یا gimp و … ویرایش میکنه و ماهرانه کد را در دستش قرار میده و با استفاده از این روش پروسه بازیابی گذرواژه اینستاگرام را دور میزنه!  
البته این فقط برای حساب‌هایی کار میکنه که توش از خود شخص عکس وجود داشته باشه و متأسفانه نمیشه جلوش رو هم گرفت و فقط حساب‌های verify شده \(تیک آبی\) میتونن غیر فعالش کنن.

۷- استفاده از گذرواژه های یکسان در سرویس ها مختلف:  
در این روش شخصی که موردنفوذ قرار می گیره معمولا در همه صفحاتش و همه سرویس‌ها \(مثلا قفل تلگرام و قفل صفحه و حساب اینستاگرام و گوگل \) از یک گذرواژه مشترک استفاده می‌کنه که اگه به هر دلیلی یکی از اونا لو بره رمز بقیه حساب ها \(که می‌توان یکی از آنها اینستاگرام باشد\) هم لو میره. پس در تعیین گذرواژه دقت کنین!!!

در نهایت به چند نکته دقت کنید:  
۱- برای حفظ امنیت خود از گذرواژه های یک‌سان و قابل حدس استفاده نکنید.  
۲- به هیچ‌عنوان به نرم‌افزار هایی که اعتماد ندارید نصب نکنید. مخصوصا نرم‌افزارهایی که به صورت رایگان به شما خدماتی عجیبی می‌دهند!!!  
۳-برای اینکه از امنیت نرم‌افزار هایی که استفاده می‌کنید مطلع بشید وبسایت های زیر رو مشاهده کنید:  
https://www.gnu.org/home.fa.html  
http://stallman.org/



